FROM    jenkins/jenkins:alpine
USER    root
RUN     apk add --update  autoconf \
						              bash \
                          build-base \
                          bison \
                          bzip2 \
                          bzip2-dev \
                          ca-certificates \
                          coreutils \
                          dpkg-dev dpkg \
                          docker \
                          gcc \
                          gdbm-dev \
                          glib-dev \
                          libc-dev \
                          libffi-dev \
                          libressl \
                          libressl-dev \
                          libxml2-dev \
                          libxslt-dev \
                          linux-headers \
                          make \
                          ncurses-dev \
                          procps \
                          readline-dev \
                          ruby \
                          ruby-bundler \
                          ruby-dev \
                          tar \
                          xz \
                          yaml-dev \
                          zlib-dev
#USER    jenkins
